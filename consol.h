#ifndef CONSOL_H
#define CONSOL_H

#include <stdio.h>
#include "inter.h"
#include "dichotomy.h"

int consol_start(int argc, char *argv[]);
void help();

#endif
