#ifndef QUAD_H
#define QUAD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Queue
{
    int err;
    char* data;
    struct Queue* next;
}Queue;

void push_Queue(Queue** a, char* val, int err);
char* pop_Queue(Queue** a, int* err);

#endif // QUAD_H
