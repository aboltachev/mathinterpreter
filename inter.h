#ifndef INTET_1
#define INTET_1

#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <stdlib.h>
#include <locale.h>
#include "list.h"
#include "parcer.h"
#include "calculate.h"
#include "dichotomy.h"

void start(void);
void instruct1(void);
void begin(List*);

#endif
