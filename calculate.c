#include "calculate.h"
#include "simpson.h"


double factorial(double a, double b)
{
    double l = b - a;
    double f1 = 0, f2 = 0;

    if(a == b)
        return a;
    if(l == 1)
        return a*b;
    if((int)l%2 != 0)
    {
        f1 = factorial(a, a+(int)(l/2)+1);
        f2 = factorial(a+(int)(l/2)+2, b);
        return f1*f2;
    }
    else
    {
        f1 = factorial(a, a+(l/2));
        f2 = factorial(a+(l/2)+1, b);
        return f1*f2;
    }
}

double calculate(Queue* input)
{
    STACK2 *stack = NULL;

    double firstN, secondN, res = 0;
    int error = 0;

   while(1)
    {

        if((input->data[0]>='0')&&(input->data[0]<='9'))
        {
            push2(&stack, atof(input->data));
            res = atof(input->data);
        }
        else if(((input->data[1]>='0')&&(input->data[1]<='9'))&&(input->data[0]=='-'))
        {
            push2(&stack, atof(input->data));
            res = atof(input->data);
        }
        else if(input->data[0]=='+')
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(stack!=NULL)
            secondN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = firstN+secondN;
            push2(&stack, res);
        }
        else if(input->data[0]=='-')
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(stack!=NULL)
            secondN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = secondN-firstN;
            push2(&stack, res);
        }
        else if(input->data[0]=='*')
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(stack!=NULL)
            secondN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = firstN*secondN;
            push2(&stack, res);
        }
        else if(input->data[0]=='/')
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(stack!=NULL)
            secondN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(secondN == 0)
            {
                printf("Error: Division by zero.");
                break;
            }
            res = secondN/firstN;
            push2(&stack, res);
        }
        else if(input->data[0]=='^')
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(stack!=NULL)
            secondN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = pow(secondN, firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"cos")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = cos(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"sin")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = sin(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"tan")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = tan(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"abs")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = fabs(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"exp")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = exp(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"asn")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = asin(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"acs")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = acos(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"atn")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = atan(firstN);
            push2(&stack, res);
        }

        else if(strcmp(input->data,"snh")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = sinh(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"csh")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = cosh(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"tnh")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = tanh(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"log")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(firstN>0)
            {
            res = log(firstN);
            push2(&stack, res);
            }
            else
            {
                printf("Error : log(n), n<0.");
                break;
            }
        }
        else if(strcmp(input->data,"log10")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(firstN>0)
            {
            res = log10(firstN);
            push2(&stack, res);
            }
            else
            {
                printf("Error : log10(n), n<0.");
                break;
            }
        }
        else if(strcmp(input->data,"fac")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = factorial(2, firstN);
            push2(&stack, res);
        }
        else if (strcmp(input->data,"integ")==0)
        {
            input = input->next;
            Queue *qInt = NULL;

            while(input->data[0]!='S')
            {
                push_Queue(&qInt, input->data, input->err);
                input = input->next;
            }

            int i = 2;

            while(input->data[i]!=')')
            {
                i++;
            }
            int i1 = i;

            char *a = malloc((i-1)*sizeof(char));
            i--;

            while(i!=1)
            {
                a[i-2] = input->data[i];
                i--;
            }

            i1+=1;
            i = 0;

            while(input->data[i1+i]!=')')
            {
                i++;
            }
            i--;


            char *b = malloc((i+1)*sizeof(char));


            while(i!=0)
            {
                b[i-1]= input->data[i1+i];
                i--;
            }
            res = simpson(qInt, atof(a),atof(b));
            push2(&stack, res);
        }

        if(input->next == NULL)
        {
            return res;
            break;
        }
        else
        {
        input = input->next;
        }
    }

}
double _calculate(Queue* input, double x)
{
    STACK2 *stack = NULL;
    int *error = 0;

    double firstN, secondN, res = 0;

   while(1)
    {

        if((input->data[0]>=97)&&(input->data[0]<=122)&&(input->data[1]==NULL))
        {
            push2(&stack, x);
            res = x;
        }
        else if((input->data[0]=='-')&&(input->data[1]>=97)&&(input->data[1]<=122))
        {
            push2(&stack, -x);
            res = -x;
        }
        else if((input->data[0]>='0')&&(input->data[0]<='9'))
        {
            push2(&stack, atof(input->data));
            res = atof(input->data);
        }
        else if(((input->data[1]>='0')&&(input->data[1]<='9'))&&(input->data[0]=='-'))
        {
            push2(&stack, atof(input->data));
            res = atof(input->data);
        }
        else if(input->data[0]=='+')
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(stack!=NULL)
            secondN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = firstN+secondN;
            push2(&stack, res);
        }
        else if(input->data[0]=='-')
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(stack!=NULL)
            secondN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = secondN-firstN;
            push2(&stack, res);
        }
        else if(input->data[0]=='*')
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(stack!=NULL)
            secondN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = firstN*secondN;
            push2(&stack, res);
        }
        else if(input->data[0]=='/')
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(stack!=NULL)
            secondN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(secondN == 0)
            {
                printf("Error: Division by zero.");
                break;
            }
            res = secondN/firstN;
            push2(&stack, res);
        }
        else if(input->data[0]=='^')
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(stack!=NULL)
            secondN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = pow(secondN, firstN);

            push2(&stack, res);
        }
        else if(strcmp(input->data,"cos")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = cos(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"sin")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = sin(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"tan")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = tan(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"abs")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = fabs(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"exp")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = exp(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"asn")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = asin(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"acs")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = acos(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"atn")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = atan(firstN);
            push2(&stack, res);
        }

        else if(strcmp(input->data,"snh")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = sinh(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"csh")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = cosh(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"tnh")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = tanh(firstN);
            push2(&stack, res);
        }
        else if(strcmp(input->data,"log")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(firstN>0)
            {
            res = log(firstN);
            push2(&stack, res);
            }
            else
            {
                printf("Error : log(n), n<0.");
                break;
            }
        }
        else if(strcmp(input->data,"log10")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            if(firstN>0)
            {
            res = log10(firstN);
            push2(&stack, res);
            }
            else
            {
                printf("Error : log10(n), n<0.");
                break;
            }
        }
        else if(strcmp(input->data,"fac")==0)
        {
            if(stack!=NULL)
            firstN = pop2(&stack);
            else
            {
                printf("Error : Many operations.\n");
                break;
            }
            res = factorial(2, firstN);
            push2(&stack, res);
        }
        else if (strcmp(input->data,"integ")==0)
        {
            input = input->next;
            Queue *qInt = NULL;

            while(input->data[0]!='S')
            {
                push_Queue(&qInt, input->data, input->err);
                input = input->next;
            }

            int i = 2;

            while(input->data[i]!=')')
            {
                i++;
            }
            int i1 = i;

            char *a = malloc((i-1)*sizeof(char));
            i--;

            while(i!=1)
            {
                a[i-2] = input->data[i];
                i--;
            }

            i1+=1;
            i = 0;

            while(input->data[i1+i]!=')')
            {
                i++;
            }
            i--;


            char *b = malloc((i+1)*sizeof(char));


            while(i!=0)
            {
                b[i-1]= input->data[i1+i];
                i--;
            }
            res = simpson(qInt, atof(a),atof(b));
            push2(&stack, res);
        }

        if(input->next == NULL)
        {
            return res;
            break;
        }
        else
        {
        input = input->next;
        }
    }

}
