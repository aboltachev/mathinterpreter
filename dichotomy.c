#include "calculate.h"
#include "math.h"

void dichotomy(double a, double b, Queue *input)
{
    double eps = 0.0000001;
    double c = (b+a)/2;

    double fa = _calculate(input, a);
    double fb = _calculate(input, b);
    double fc = _calculate(input, c);
    while (fabs(fc) >= eps)
    {
        if(fa*fc < 0)
        {
            b = c;
            fb = fc;
        }
        else if (fc*fb < 0)
        {
            a = c;
            fa = fc;
        }
        else
        {
            printf("Корней нет\n");
            return;
        }
        c = (a + b) / 2;
        fc = _calculate(input, c);
    }
    printf("%lf", c);
}
