#ifndef STACK_H
#define STACK_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct STACK2
{
    double a;
    struct STACK2* next;
}STACK2;

typedef struct STACK1
{
    int pos;
    int a;
    char* data;
    struct STACK1* next;
}STACK1;

void push1(STACK1** s, char* str, int l, int pos);
char* pop1(STACK1** s, int* pos);
void push2(STACK2** s, double l);
double pop2(STACK2** s);

#endif // STACK_H
