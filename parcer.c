#include "parcer.h"

Queue *parcer(char *sr, List *ls, int isEquat)
{
    char varib_e = '\0';
    char varib_i = '\0';
    isInteg = 0;
    pos = 0;
    str = (char*)malloc(strlen(sr) + 1);
    strcpy(str, sr);
    char ch, ch_p = '(';
    q = NULL;
    STACK1 *s = NULL;
    ch = getChar();
    while (ch != '\0')
    {
        if (ch == '-' && !isDigit(ch_p) && !isCharacter(ch_p))
        {
            ch = getChar();
            if (isCharacter(ch))
            {
                char *st = find_and_pop(&ls, ch);
                if (st == NULL)
                {
                    if (isInteg)
                    {
                        if (varib_i == '\0')
                            varib_i = ch;
                        else if (varib_i != ch)
                        {
                            Error(sr, pos - 1);
                            return NULL;
                        }
                        char t[3];
                        t[0] = '-';
                        t[1] = ch;
                        t[2] = '\0';
                        push_Queue(&q, t, pos - 1);
                    }
                    else if (isEquat)
                    {
                        if (varib_e == '\0')
                            varib_e = ch;
                        else if (varib_e != ch)
                        {
                            Error(sr, pos - 1);
                            return NULL;
                        }
                        char t[3];
                        t[0] = '-';
                        t[1] = ch;
                        t[2] = '\0';
                        push_Queue(&q, t, pos - 1);
                    }
                    else
                    {
                        Error(sr, pos - 1);
                        return NULL;
                    }
                }
                else
                {
                    char *s_t = (char*)malloc(strlen(st)+2);
                    strcpy(s_t+1, st);
                    s_t[0] = '-';
                    push_Queue(&q, s_t, pos - 1);
                    free(st);
                    free(s_t);
                }
            }
            else
            {
                returnChar(ch);
                returnChar('-');
                char *st = getValue();
                if (st == NULL)
                {
                    Error(sr, pos + 1);
                    return NULL;
                }
                push_Queue(&q, st, pos + 1 - strlen(st));
                free(st);
            }
        }
        else if (isDigit(ch))
        {
            returnChar(ch);
            char *st = getValue();
            if (st == NULL)
            {
                Error(sr, pos);
                return NULL;
            }
            push_Queue(&q, st, pos - strlen(st));
            free(st);
        }
        else if (isCharacter(ch))
        {
            if (!isCharacter(str[0]) && str[0] != '(')
            {
                char *st = find_and_pop(&ls, ch);
                if (st == NULL)
                {
                    if (isInteg)
                    {
                        if (varib_i == '\0')
                            varib_i = ch;
                        else if (varib_i != ch)
                        {
                            Error(sr, pos - 1);
                            return NULL;
                        }
                        char t[2];
                        t[0] = ch;
                        t[1] = '\0';
                        push_Queue(&q, t, pos - 1);
                    }
                    else if (isEquat)
                    {
                        if (varib_e == '\0')
                            varib_e = ch;
                        else if (varib_e != ch)
                        {
                            Error(sr, pos - 1);
                            return NULL;
                        }
                        char t[2];
                        t[0] = ch;
                        t[1] = '\0';
                        push_Queue(&q, t, pos - 1);
                    }
                    else
                    {
                        Error(sr, pos - 1);
                        return NULL;
                    }
                }
                else
                {
                    push_Queue(&q, st, pos - strlen(st));
                    free(st);
                }
            }
            else
            {
                returnChar(ch);
                char *st = getFunc();
                if (st == NULL)
                {
                    Error(sr, pos);
                    return NULL;
                }
                push1(&s, st, 1, pos - strlen(st));
                free(st);
                ch = '(';
            }
        }
        else if (ch == '(')
        {
            char *t = (char*)malloc(2);
            t[0] = ch;
            t[1] = '\0';
            push1(&s, t, _true(ch, 0), pos - 1);
            free(t);
        }
        else if (ch == ')')
        {
            int t_p;
            char *temp;
            temp = pop1(&s, &t_p);
            if (temp == NULL)
            {
                Error(sr, pos - 1);
                return NULL;
            }
            while (temp[0] != '(')
            {
                push_Queue(&q, temp, t_p);
                free(temp);
                temp = pop1(&s, &t_p);
                if (temp == NULL)
                {
                    Error(sr, pos - 1);
                    return NULL;
                }
            }
            if (strlen(temp) > 1)
            {
                if (temp[1] == 'S' && temp[2] == '(')
                    isInteg = 0;
                char *st = (char*)malloc(strlen(temp));
                strcpy(st, temp + 1);
                free(temp);
                push_Queue(&q, st, t_p);
                free(st);
            }
        }
        else if (ch == '+' || ch == '-' || ch == '*' || ch == '/')
        {
            int t_p;
            while ((s != NULL) && (_true(ch, s->a) <= 0))
            {
                char *t = pop1(&s, &t_p);
                push_Queue(&q, t, t_p);
            }
            char *t = (char*)malloc(2);
            t[0] = ch;
            t[1] = '\0';
            push1(&s, t, _true(ch, 0), pos - 1);
            free(t);
        }
        else if (ch == '^')
        {
            int t_p;
            while ((s != NULL) && (_true(ch, s->a) < 0))
            {
                char *t = pop1(&s, &t_p);
                push_Queue(&q, t, t_p);
            }
            char *t = (char*)malloc(2);
            t[0] = ch;
            t[1] = '\0';
            push1(&s, t, _true(ch, 0), pos - 1);
            free(t);
        }
        ch_p = (ch == ')') ? '0' : ch;
        ch = getChar();
    }
    while (s != NULL)
    {
        char *st = pop1(&s, &pos);
        if (st[0] == '(')
        {
            Error(sr, pos);
            return NULL;
        }
        push_Queue(&q, st, pos);
    }
    return q;
}

int isDigit(char ch)
{
    if (ch >= '0' && ch <= '9')
        return 1;
    return 0;
}

void returnChar(char ch)
{
    char *s_t = (char*)malloc(strlen(str) + 2);
    strcpy(s_t + 1, str);
    s_t[0] = ch;
    free(str);
    str = s_t;
    pos--;
}

char getChar()
{
    char* s_t = (char*)malloc(strlen(str));
    char t = str[0];
    strcpy(s_t, str + 1);
    free(str);
    str = s_t;
    pos++;
    return t;
}

char *getValue()
{
    int i = 0;
    int number_of_point = 0;
    if (str[0] == '-')
        i++;
    while (str[i] != '\0' && ((isDigit(str[i]) || str[i] == ',')))
    {
        if (str[i] == ',' && number_of_point <= 1)
            number_of_point++;
        else if (str[i] == ',')
            return NULL;
        i++;
    }
    if (str[i] != '+' && str[i] != '-' && str[i] != '*' && str[i] != '/' && str[i] != '^' && str[i] != ')' && str[i] != '\0')
        return NULL;
    char *s_t = (char*)malloc(i+1);
    strncpy(s_t, str, i);
    s_t[i] = '\0';
    char *temp = (char*)malloc(strlen(str)-i+1);
    strcpy(temp, str + i);
    free(str);
    str = temp;
    pos += i;
    return s_t;
}

int _true(char ch, int i)
{
    switch (ch)
    {
    case '(':
    case ')':
        if (i == 1)
            return 0;
        if (i > 1)
            return -1;
        return 1;

    case '+':
    case '-':
        if (i == 2)
            return 0;
        if (i > 2)
            return -1;
        return 2;

    case '*':
    case '/':
        if (i == 3)
            return 0;
        if (i > 3)
            return -1;
        return 3;

    case '^':
        if (i == 4)
            return 0;
        if (i > 4)
            return -1;
        return 4;
    }
    return 10;
}

char *getFunc()
{
    char *s_t;
    s_t = isFunc("log10", 5);
    if (s_t != NULL)
        return s_t;
    s_t = isFunc("sin", 3);
    if (s_t != NULL)
        return s_t;
    s_t = isFunc("cos", 3);
    if (s_t != NULL)
        return s_t;
    s_t = isFunc("tan", 3);
    if (s_t != NULL)
        return s_t;
    s_t = isFunc("snh", 3);
    if (s_t != NULL)
        return s_t;
    s_t = isFunc("csh", 3);
    if (s_t != NULL)
        return s_t;
    s_t = isFunc("tnh", 3);
    if (s_t != NULL)
        return s_t;
    s_t = isFunc("asn", 3);
    if (s_t != NULL)
        return s_t;
    s_t = isFunc("acs", 3);
    if (s_t != NULL)
        return s_t;
    s_t = isFunc("atn", 3);
    if (s_t != NULL)
        return s_t;
    s_t = isFunc("exp", 3);
    if (s_t != NULL)
        return s_t;
    s_t = isFunc("abs", 3);
    if (s_t != NULL)
        return s_t;
    s_t = isFunc("fac", 3);
    if (s_t != NULL)
        return s_t;
    s_t = isFunc("log", 3);
    if (s_t != NULL)
        return s_t;
    if (!isInteg)
    {
        int i, j, number_of_point = 0;
        char a[20], b[20];
        s_t = (char*)malloc(3);
        strncpy(s_t, str, 2);
        s_t[2] = '\0';
        if (strcmp(s_t, "S("))
            return NULL;
        i = 2;
        j = 0;
        if (str[i] == '-')
            a[j++] = str[i++];
        while (str[i] != ')' && ((isDigit(str[i]) || str[i] == ',')))
        {
            if (str[i] == ',' && number_of_point >= 1)
                number_of_point++;
            else if (str[i] == ',')
                return NULL;
            if (str[i] == '\0')
                return NULL;
            a[j++] = str[i++];
        }
        a[j] = '\0';
        if (str[++i] != '(')
            return NULL;
        i++;
        j = 0;
        if (str[i] == '-')
            b[j++] = str[i++];
        while (str[i] != ')' && ((isDigit(str[i]) || str[i] == ',')))
        {
            if (str[i] == ',' && number_of_point >= 1)
                number_of_point++;
            else if (str[i] == ',')
                return NULL;
            if (str[i] == '\0')
                return NULL;
            b[j++] = str[i++];
        }
        b[j] = '\0';
        if (str[++i] != '(')
            return NULL;
        if (atof(a) < atof(b))
        {
            free(s_t);
            s_t = (char*)malloc(i+2);
            strcpy(s_t, "(S(");
            strcat(s_t, a);
            strcat(s_t, ")(");
            strcat(s_t, b);
            strcat(s_t, ")");
            char *temp = (char*)malloc(strlen(str)-strlen(s_t)+1);
            strcpy(temp, str + strlen(s_t));
            free(str);
            pos += strlen(s_t);
            str = temp;
            isInteg = 1;
            push_Queue(&q, "integ", 0);
            return s_t;
        }
    }
    free(s_t);
    return NULL;
}

int isCharacter(char ch)
{
    if (ch >= 'a' && ch <= 'z')
        return 1;
    if (ch >= 'A' && ch <= 'Z')
        return 1;
    return 0;
}

char *isFunc(char *s, int p)
{
    if (strlen(str) < p)
        return NULL;
    char *s_t = (char*)malloc(p+1);
    strncpy(s_t, str, p);
    s_t[p] = '\0';
    if (!strcmp(s_t, s))
    {
        pos += p + 1;
        char *temp = (char*)malloc(strlen(str)-p);
        strcpy(temp, str + p + 1);
        free(str);
        str = temp;
        free(s_t);
        s_t = (char*)malloc(p + 2);
        strcpy(s_t, "(");
        strcat(s_t, s);
        return s_t;
    }
    free(s_t);
    return NULL;
}
