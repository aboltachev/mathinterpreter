#ifndef calculate_h
#define calculate_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"
#include "queue.h"
#include <math.h>

double _calculate(Queue* input, double x);
double calculate(Queue* input);

#endif
